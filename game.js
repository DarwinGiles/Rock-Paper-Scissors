var options = ["rock", "paper", "scissors"]
var roundNumber = 0;
var playerWins = 0;
var computerWins = 0;
var lastRoundResult = "Click a button to make your choice!";

function computerPlay()
{
    return options[Math.floor(Math.random() * 3)]
}

function playRound(playerSelection, computerSelection)
{
    if (playerSelection == computerSelection)
    {
        return "It's a tie!"
    }
    else
    {
        var playerIdx = options.indexOf(playerSelection);
        var computerIdx = options.indexOf(computerSelection);

        var computerWin = ((playerIdx + 1) % 3) == computerIdx;

        if (computerWin)
        {
            return "You lose!";
        }
        else
        {
            return "You win!";
        }
    }
}

function playerPlay()
{
    var answer = "";

    while (options.indexOf(answer) == -1)
    {
        answer = prompt("Enter your choice.").toLowerCase();
    }

    return answer;
}

function game()
{
    var playerWins = 0;
    var computerWins = 0;

    for (i = 0; i < 5; i++)
    {
        var playerChoice = playerPlay();
        var computerChoice = computerPlay();

        var result = playRound(playerChoice, computerChoice);
        alert("The computer played " + computerChoice + "\n" + result);
    }
}

function updateGui()
{
    document.querySelector("#round-counter").textContent = "Round #" + roundNumber.toString();
    document.querySelector("#player-score").textContent = playerWins.toString();
    document.querySelector("#computer-score").textContent = computerWins.toString();
    document.querySelector("#results-text").textContent = lastRoundResult;
}

function onClick(e)
{
    var playerChoice = this.dataset.move;
    var computerChoice = computerPlay();

    var result = playRound(playerChoice, computerChoice);
    lastRoundResult = ("The computer played " + computerChoice + "\n" + result);

    switch(result)
    {
        case "It's a tie!":
        break;
        case "You win!":
        playerWins += 1;
        break;
        case "You lose!":
        computerWins += 1;
        break;
    }

    roundNumber += 1;
    updateGui()

    var lastSelected = document.querySelector('.selected');

    console.log(lastSelected);
    if (lastSelected != null)
    {
        lastSelected.classList.remove("selected");
    }
    this.classList.add("selected");

    if (roundNumber > 5)
    {
        var result = (computerWins == playerWins) ? "It's a tie!" : (playerWins > computerWins) ? "You win!" : "You lose!"
        alert("Game Over!\n" + result + "\n" + playerWins.toString() + " to " + computerWins.toString())
        initialize()
    }
}

function initialize()
{
    roundNumber = 1;
    playerWins = 0;
    computerWins = 0;
    lastRoundResult = "Click a button to make your choice!";

    updateGui()

    var buttons = document.querySelectorAll("button");
    buttons.forEach(button => button.addEventListener("click", onClick))
}

initialize();
