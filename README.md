# Rock Paper Scissors

A project where you play Rock Paper Scissors against a computer.

Built as part of The Odin Project's [curriculum.](https://www.theodinproject.com/courses/web-development-101/lessons/rock-paper-scissors?ref=lnav)
